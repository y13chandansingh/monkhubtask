package com.example.chandan.monkhubtask;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by chandan on 22/12/17.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private int noOfTabs;

    public ViewPagerAdapter(FragmentManager fm , int noOfTabs) {
        super(fm);
        this.noOfTabs = noOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ColorsFragment tab1 = new ColorsFragment();
                return tab1;
            case 1:
                PhrasesFragment tab2 = new PhrasesFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return noOfTabs;
    }
}
