package com.example.chandan.monkhubtask;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class ColorsFragment extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerAdapter mAdapter;
    private ArrayList<String> dataList = new ArrayList<>();
    private ArrayList<String> dataListTranslation = new ArrayList<>();

    public static ColorsFragment newInstance() {
        ColorsFragment fragment = new ColorsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_colors, container, false);
        makeData();
        recyclerView = view.findViewById(R.id.rvDetail);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new RecyclerAdapter(getActivity() , dataList , dataListTranslation);
        recyclerView.setAdapter(mAdapter);
        return view;
    }

    public void makeData() {
        for (int i =0 ; i < 10 ; i++) {
            dataList.add(i ,"Language Word "+i);
            dataListTranslation.add(i , "English Translation "+i);
        }
    }


}
