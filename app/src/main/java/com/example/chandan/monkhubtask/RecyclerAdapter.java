package com.example.chandan.monkhubtask;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by chandan on 22/12/17.
 */

public class RecyclerAdapter extends RecyclerView.Adapter {
    private Context ctx;
    ArrayList<String> dataList = new ArrayList<>();
    ArrayList<String> dataListTranslation = new ArrayList<>();

    public RecyclerAdapter(Context mainActivity, ArrayList<String> dataList, ArrayList<String> dataListTranslation) {
        this.ctx = mainActivity;
        this.dataList = dataList;
        this.dataListTranslation = dataListTranslation;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(ctx)
                .inflate(R.layout.row_layout_colors, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MyViewHolder)holder).onBind(position);
    }


    @Override
    public int getItemCount() {
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtViewLanguage;
        public TextView txtViewTranslation;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtViewLanguage = itemView.findViewById(R.id.tv1);
            txtViewTranslation = itemView.findViewById(R.id.tv2);
        }

        public void onBind(int position) {
            txtViewLanguage.setText(dataList.get(position));
            txtViewTranslation.setText(dataListTranslation.get(position));
        }
    }
}
